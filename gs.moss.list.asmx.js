/*
	gs.moss.list.asmx.js
	Licence: GPL v3
	Copright: Gestalt Systems Ltd <http://www.gestaltsystems.co.uk/>
	Current Version: 1.3 
	
	The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
// SOAP Definitions
var xmlGetListSOAP = '<?xml version="1.0" encoding="utf-8"?>\
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\
  <soap:Body>\
    <GetList xmlns="http://schemas.microsoft.com/sharepoint/soap/">\
      <listName>#listname#</listName>\
    </GetList>\
  </soap:Body>\
</soap:Envelope>';

var xmlGetListItemsSOAP = '<?xml version="1.0" encoding="utf-8"?>\
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\
  <soap:Body>\
    <GetListItems xmlns="http://schemas.microsoft.com/sharepoint/soap/">\
      <listName>#ID#</listName>\
      <viewName>#viewname#</viewName>\
      #query#\
      #queryOptions#\
    </GetListItems>\
  </soap:Body>\
</soap:Envelope>';
//Functions

function getListItemsByListName(Name, listProcessor, viewName, url, query, queryOptions){
        var reUrl = "";
        if(url != null)
                relUrl = url + "_vti_bin/lists.asmx";
        else
                relUrl = "_vti_bin/lists.asmx";
        if(query == null)
                query = "";
        if(queryOptions == null)
                queryOptions = "";

        var view = viewName;
        var listNameString = Name;
        var xmlPost = xmlGetListSOAP.replace("#listname#",listNameString);
        
        var request1 = new XMLHttpRequest();
        request1.open("POST",relUrl);
        request1.onreadystatechange = function(){
                if(request1.readyState === 4){
                        var request2 = new XMLHttpRequest();
                        var list = request1.responseXML;
                        var listAttr = list.getElementsByTagName("List");
								var listId = listAttr[0].getAttribute("ID").toString();
								
								
                        var xmlPOST = xmlGetListItemsSOAP.replace("#ID#",listId);
                        xmlPOST = xmlPOST.replace("#viewname#",view);
                        xmlPOST = xmlPOST.replace("#query#",query);
                        xmlPOST = xmlPOST.replace("#queryOptions",queryOptions);
                        request2.onreadystatechange = function(){
                                if(request2.readyState === 4){
                                        listProcessor(request2.responseXML);
                                }
			}
                        request2.open("POST", relUrl);
                        request2.setRequestHeader("Content-Type","text/xml;charset=UTF-8");
                        request2.send(xmlPOST);
                        
                        }
                        
                }
        request1.setRequestHeader("Content-Type","text/xml;charset=UTF-8");
        request1.send(xmlPost);
}
 
